package main
    
import (
	"fmt"
	"time"
	"strconv"
	"math/rand"
	"sort"
)

type Animal struct {
	phylum string
	direction int // 0 = N, clockwise 45 degrees per
}

type Cell struct {
	animals []Animal
}
   
func main() {
	var run_catagolue = true
	var run_technoblade = true
	if (run_catagolue) {
		if run_technoblade {
			technoblade()
		} else {
			catagolue()
		}
	} else {
		// {"0", "0", "0", "0", "0", "0", "0"},
		var pat = [][]string{
			{"0", "f5", "w5", "0", "f3", "0", "f5", "0", "g5", "0"},
{"0", "0", "0", "0", "0", "0", "0", "0", "0", "0"},
{"0", "0", "g3", "0", "0", "0", "0", "w7", "0", "f5"},
{"0", "0", "0", "w7", "0", "0", "0", "w3", "0", "0"},
{"g7", "0", "0", "0", "0", "w7", "0", "0", "g1", "f5"},
{"0", "0", "0", "0", "w1", "0", "0", "0", "g5", "0"},
{"g3", "g3", "0", "0", "0", "g3", "0", "w3", "0", "g7"},
{"0", "0", "g7", "0", "0", "w5", "0", "f1", "0", "0"},
{"g1", "0", "0", "0", "0", "0", "g5", "0", "0", "0"},
{"0", "0", "w3", "f3", "w5", "0", "0", "f5", "f1", "0"},
}
		var soup = generate(pat)
		print(soup)
		for g := 0; g < 10000; g++ {
			fmt.Println("---------------------")
			soup = advance(soup)
			fmt.Print("\033[H\033[2J")
			print(soup)		
			time.Sleep(250*time.Millisecond)
		}
	}
}

func catagolue() {
	var num_soups = 100000
	var num_cutoff_generations = 100

	var cutoff_pats [][][]string
	var interesting_pats [][][]string
	var period_census map[int]int = make(map[int]int)

	var width = 6
	var height = 6

	var current_time = time.Now()
	var time_counter = 0;

	for i := 0; i < num_soups; i++ {
		var pat = make([][]string, height)
		for h := 0; h < height; h++ {
			pat[h] = make([]string, width)
			for w := 0; w < width; w++ {
				pat[h][w] = "0"
			}
		}
		var num_animals = rand.Intn(width * height)
		for a := 0; a < num_animals; a++ {
			var a_typen = rand.Intn(3)
			var a_direction = rand.Intn(4)*2+1
			var a_i = rand.Intn(height)
			var a_j = rand.Intn(width)
			var a_type string
			if a_typen == 0 {
				a_type = "g"
			} else if a_typen == 1 {
				a_type = "f"
			} else {
				a_type = "w"
			}
			pat[a_i][a_j] = a_type + strconv.Itoa(a_direction)
		}
		var soup = generate(pat)
		for g := 0; g <= num_cutoff_generations; g++ {
			if has_won(soup) != "NOBODY" {
				break
			}
			if g == num_cutoff_generations {
				if is_interesting(soup) {
					cutoff_pats = append(cutoff_pats, pat)
					interesting_pats = append(interesting_pats, pat)
				}
			}
			soup = advance(soup)
		}
		if (time.Now().After(current_time.Add(5*time.Second))) {
			time_counter += 5
			current_time = time.Now()
			fmt.Println(strconv.Itoa(i) + " soups processed (" + strconv.Itoa(time_counter) + "s)")
		}
	}
	for i := 0; i < len(interesting_pats); i++ {
		fmt.Println("---------------------")
		print(generate(interesting_pats[i]))
		var period = periodicity(generate(interesting_pats[i]))
		period_census[period] += 1
		fmt.Println("Periodicity: " + strconv.Itoa(period))
		fmt.Println("Pat: " + get_pat_cp(interesting_pats[i]))
		fmt.Println("Stabilized pat: " + get_stabilized_pat_cp(interesting_pats[i]))
		fmt.Println("---------------------")
	}
	fmt.Println("Periods:")
	print_sorted_map(period_census)
}

func technoblade() {
	var num_cutoff_generations = 100

	var cutoff_pats [][][]string
	var interesting_pats [][][]string
	var period_census map[int]int = make(map[int]int)

	var width = 5
	var height = 5

	var current_time = time.Now()
	var time_counter = 0;

	for i := 0; true; i++ {
		width = rand.Intn(9) + 2
		height = rand.Intn(9) + 2
		var pat = make([][]string, height)
		for h := 0; h < height; h++ {
			pat[h] = make([]string, width)
			for w := 0; w < width; w++ {
				pat[h][w] = "0"
			}
		}
		var num_animals = rand.Intn(width * height)
		for a := 0; a < num_animals; a++ {
			var a_typen = rand.Intn(3)
			var a_direction = rand.Intn(4)*2+1
			var a_i = rand.Intn(height)
			var a_j = rand.Intn(width)
			var a_type string
			if a_typen == 0 {
				a_type = "g"
			} else if a_typen == 1 {
				a_type = "f"
			} else {
				a_type = "w"
			}
			pat[a_i][a_j] = a_type + strconv.Itoa(a_direction)
		}
		var soup = generate(pat)
		for g := 0; g <= num_cutoff_generations; g++ {
			if has_won(soup) != "NOBODY" {
				break
			}
			if g == num_cutoff_generations {
				if is_interesting(soup) {
					cutoff_pats = append(cutoff_pats, pat)
					if is_interesting_pass2(soup) {
						interesting_pats = append(interesting_pats, pat)
						goto process
					}
				}
			}
			soup = advance(soup)
		}
		if (time.Now().After(current_time.Add(5*time.Second))) {
			time_counter += 5
			current_time = time.Now()
			fmt.Println(strconv.Itoa(i) + " soups processed (" + strconv.Itoa(time_counter) + "s)")
			if time_counter % 10 == 0 {
				for i := 0; i < len(cutoff_pats); i++ {
					var period = periodicity(generate(cutoff_pats[i]))
					period_census[period] += 1
				}
				cutoff_pats = nil

				fmt.Println("Periods:")
				print_sorted_map(period_census)
				fmt.Println("---------------------")
			}
		}
	}
	process:
	for i := 0; i < len(interesting_pats); i++ {
		fmt.Println("---------------------")
		print(generate(interesting_pats[i]))
		var period = periodicity(generate(interesting_pats[i]))
		period_census[period] += 1
		fmt.Println("Periodicity: " + strconv.Itoa(period))
		fmt.Println("Pat: " + get_pat_cp(interesting_pats[i]))
		fmt.Println("Stabilized pat: " + get_stabilized_pat_cp(interesting_pats[i]))
		fmt.Println("---------------------")
	}
	fmt.Println("Periods:")
	print_sorted_map(period_census)
}

func print_sorted_map(_map map[int]int) {
	keys := make([]int, len(_map))
	for k := range _map{
		keys = append(keys, k)
    }
    sort.Ints(keys)
    for _, k := range keys {
		if k != 0 {
			fmt.Println(k, _map[k])
		}
    }
}

func generate(pat [][]string) ([][]Cell){
	var soup = make([][]Cell, len(pat))
	for i := 0; i < len(pat); i++ {
		soup[i] = make([]Cell, len(pat[0]))
	}
	for i := 0; i < len(pat); i++ {
		for j := 0; j < len(pat[i]); j++ {
			if pat[i][j] == "0" {
				continue
			}
			var animal = new(Animal)
			if (pat[i][j][0] == 'f') {
				animal.phylum = "FIRE"
			} else if (pat[i][j][0] == 'g') {
				animal.phylum = "GRASS"
			} else if (pat[i][j][0] == 'w') {
				animal.phylum = "WATER"
			} else {
				animal.phylum = "WALL"
			}
			animal.direction = int(pat[i][j][1] - '0')
			soup[i][j].animals = append(soup[i][j].animals, *animal)
		}
	}
	return soup
}

func print(soup [][]Cell) {
	for i := 0; i < len(soup); i++ {
		for j := 0; j < len(soup[i]); j++ {
			// if (len(soup[i][j].animals) == 0) {
			// 	color.set(color.FgWhite)
			// } else if soup[i][j].animals[0].phylum == "FIRE" {
			// 	color.set(color.FgRed)
			// } else if soup[i][j].animals[0].phylum == "WATER" {
			// 	color.set(color.FgBlue)
			// } else if soup[i][j].animals[0].phylum == "GRASS" {
			// 	color.set(color.FgGreen)
			// } else {
			// 	color.set(color.FgWhite)
			// }
			// fmt.Print("X")
			fmt.Print("\033[0;0m")
			if (len(soup[i][j].animals) == 0) {
				fmt.Print(" ")
			} else if (len(soup[i][j].animals) == 3) {
				fmt.Print("\033[0;45m" + strconv.Itoa(soup[i][j].animals[0].direction))
			} else if soup[i][j].animals[0].phylum == "FIRE" {
				fmt.Print("\033[0;101m" + strconv.Itoa(soup[i][j].animals[0].direction))
			} else if soup[i][j].animals[0].phylum == "WATER" {
				fmt.Print("\033[0;104m" + strconv.Itoa(soup[i][j].animals[0].direction))
			} else if soup[i][j].animals[0].phylum == "GRASS" {
				fmt.Print("\033[0;102m" + strconv.Itoa(soup[i][j].animals[0].direction))
			} else if soup[i][j].animals[0].phylum == "WALL" {
				fmt.Print("X")
			} else {
				fmt.Print("0")
			}
		}
		fmt.Print("\033[0;0m")
		fmt.Println()
	}
}

func is_interesting(soup [][]Cell) (bool){
	// arbitrary determination
	var water_present int = 0
	var fire_present int = 0
	var grass_present int = 0
	for i := 0; i < len(soup); i++ {
		for j := 0; j < len(soup[i]); j++ {
			for a := 0; a < len(soup[i][j].animals); a++ {
				var animal = soup[i][j].animals[a]
				if (animal.phylum == "WATER") {
					water_present += 1
				} else if (animal.phylum == "FIRE") {
					fire_present += 1
				} else if (animal.phylum == "GRASS") {
					grass_present += 1
				}
			}
		}
	}
	if (water_present + fire_present + grass_present < 13) {
		return false
	}
	if (water_present != 0) {
		var ratio = float64(fire_present)/float64(water_present)
		if (ratio >= 0.2 && ratio <= 10 && water_present > 2) {
			return true
		}
		ratio = float64(grass_present)/float64(water_present) 
		if (ratio >= 0.2 && ratio <= 10 && water_present > 2) {
			return true
		}
	}
	if (fire_present != 0) {
		var ratio = float64(water_present)/float64(fire_present)
		if (ratio >= 0.2 && ratio <= 10 && fire_present > 2) {
			return true
		}
		ratio = float64(grass_present)/float64(fire_present)
		if (ratio >= 0.2 && ratio <= 10 && fire_present > 2) {
			return true
		}
	}
	if (grass_present != 0) {
		var ratio = float64(fire_present)/float64(grass_present)
		if (ratio >= 0.2 && ratio <= 10 && grass_present > 2) {
			return true
		}
		ratio = float64(water_present)/float64(grass_present)
		if (ratio >= 0.2 && ratio <= 10 && grass_present > 2) {
			return true
		}
	}
	return false
}

func is_interesting_pass2(soup [][]Cell) (bool){
	var period = periodicity(soup)
	return (period % 2 == 1) || (period == -1)
}

func periodicity(soup [][]Cell) (int){
	var initial string
	for i := 0; i < 2000; i++ {
		soup = advance(soup)
		if i == 100 {
			initial = get_soup_key(soup)
		} else if i > 100 {
			if (get_soup_key(soup) == initial) {
				return i-100
			}
		}
	}
	return periodicity(soup)
}

func periodicity_hard(soup [][]Cell) (int){
	var initial string
	for i := 0; i < 3000; i++ {
		soup = advance(soup)
		if i == 500 {
			initial = get_soup_key(soup)
		} else if i > 500 {
			if (get_soup_key(soup) == initial) {
				return i-500
			}
		}
	}
	return -1
}

func get_soup_key(soup [][]Cell) (string){
	var soup_key string = ""
	for i := 0; i < len(soup); i++ {
		for j := 0; j < len(soup[i]); j++ {
			var cell_sorted_animals []string
			for a := 0; a < len(soup[i][j].animals); a++ {
				var animal = soup[i][j].animals[a]
				cell_sorted_animals = append(cell_sorted_animals, animal.phylum + strconv.Itoa(animal.direction))
			}
			sort.Strings(cell_sorted_animals)
			for a := 0; a < len(cell_sorted_animals); a++ {
				soup_key += cell_sorted_animals[a]
			}
			soup_key += "-"
		}
		soup_key += "_"
	}
	return soup_key
}

func get_pat_cp(pat [][]string) (string){
	var pat_cp string = ""
	for i := 0; i < len(pat); i++ {
		pat_cp += "{"
		for j := 0; j < len(pat[i]); j++ {
			pat_cp += "\"" + pat[i][j] + "\""
			if (j < len(pat[i]) - 1) {
				pat_cp += ", "
			}
		}
		pat_cp += "},\n"
	}
	return pat_cp
}

func get_stabilized_pat_cp(pat [][]string) (string){
	var soup = generate(pat)
	for i := 0; i < 300; i++ {
		soup = advance(soup)
	}
	var pat_cp string = ""
	for i := 0; i < len(soup); i++ {
		pat_cp += "{"
		for j := 0; j < len(soup[i]); j++ {
			if (len(soup[i][j].animals) > 0) {
				var animal Animal = soup[i][j].animals[0]
				if animal.phylum == "FIRE" {
					pat_cp += "\"f" + strconv.Itoa(animal.direction) + "\""
				} else if animal.phylum == "WATER" {
					pat_cp += "\"w" + strconv.Itoa(animal.direction) + "\""
				} else if animal.phylum == "GRASS" {
					pat_cp += "\"g" + strconv.Itoa(animal.direction) + "\""
				} else {
					pat_cp += "\"x\""
				}
			} else {
				pat_cp += "\"0\""
			}
			if (j < len(soup[i]) - 1) {
				pat_cp += ", "
			}
		}
		pat_cp += "}, "
	}
	return pat_cp
}

func has_won(soup [][]Cell) (string){
	var water_present int = 0
	var fire_present int = 0
	var grass_present int = 0
	for i := 0; i < len(soup); i++ {
		for j := 0; j < len(soup[i]); j++ {
			for a := 0; a < len(soup[i][j].animals); a++ {
				var animal = soup[i][j].animals[a]
				if (animal.phylum == "WATER") {
					water_present = 1
				} else if (animal.phylum == "FIRE") {
					fire_present = 1
				} else if (animal.phylum == "GRASS") {
					grass_present = 1
				}
			}
		}
	}
	if (water_present + fire_present + grass_present) <= 1 {
		if (water_present == 1) {
			return "WATER"
		} else if (fire_present == 1) {
			return "FIRE"
		} else if (grass_present == 1) {
			return "GRASS"
		}
	}
	return "NOBODY"
}

func advance(soup [][]Cell) ([][]Cell){
	var new_soup = make([][]Cell, len(soup))
	for i := 0; i < len(soup); i++ {
		new_soup[i] = make([]Cell, len(soup[0]))
	}
	for i := 0; i < len(soup); i++ {
		for j := 0; j < len(soup[i]); j++ {
			for a := 0; a < len(soup[i][j].animals); a++ {
				var animal = soup[i][j].animals[a]
				var new_i, new_j, new_direction = move(soup, i, j, animal.direction)
				var new_phylum = move_consume(soup, i, j, new_direction, animal.phylum)
				var new_animal = new(Animal)
				new_animal.phylum = new_phylum
				new_animal.direction = new_direction
				new_soup[new_i][new_j].animals = append(new_soup[new_i][new_j].animals, *new_animal)
			} 	
		}
	}
	for i := 0; i < len(new_soup); i++ {
		for j := 0; j < len(new_soup[i]); j++ {
			new_soup[i][j] = consume(new_soup[i][j])
		}
	}
	return new_soup
}

func consume(cell Cell) (Cell) {
	var water_present bool = false
	var fire_present bool = false
	var grass_present bool = false
	for a := 0; a < len(cell.animals); a++ {
		var animal = cell.animals[a]
		if (animal.phylum == "WATER") {
			water_present = true
		} else if (animal.phylum == "FIRE") {
			fire_present = true
		} else if (animal.phylum == "GRASS") {
			grass_present = true
		}
	}
	if (water_present && fire_present && grass_present) {
		return cell
	}
	for a := 0; a < len(cell.animals); a++ {
		var animal = cell.animals[a]
		if (animal.phylum == "WATER" && grass_present) {
			animal.phylum = "GRASS"
		} else if (animal.phylum == "FIRE" && water_present) {
			animal.phylum = "WATER"
		} else if (animal.phylum == "GRASS" && fire_present) {
			animal.phylum = "FIRE"
		}
		cell.animals[a] = animal
	}
	return cell
}

func move_consume(soup [][]Cell, i int, j int, direction int, phylum string) (string) {
	var water_present bool = false
	var fire_present bool = false
	var grass_present bool = false
	switch direction {
	case 1:
		if (in_bounds(soup, i-1,j+1) && len(soup[i-1][j+1].animals) > 0) {
			var cell = soup[i-1][j+1]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction != 5 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
		if (in_bounds(soup, i,j+1) && len(soup[i][j+1].animals) > 0) {
			var cell = soup[i][j+1]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction == 1 || cell.animals[i].direction == 3 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
		if (in_bounds(soup, i-1,j) && len(soup[i-1][j].animals) > 0) {
			var cell = soup[i-1][j]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction == 1 || cell.animals[i].direction == 7 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
	case 3:
		if (in_bounds(soup, i+1,j+1) && len(soup[i+1][j+1].animals) > 0) {
			var cell = soup[i+1][j+1]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction != 7 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
		if (in_bounds(soup, i,j+1) && len(soup[i][j+1].animals) > 0) {
			var cell = soup[i][j+1]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction == 1 || cell.animals[i].direction == 3 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
		if (in_bounds(soup, i+1,j) && len(soup[i+1][j].animals) > 0) {
			var cell = soup[i+1][j]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction == 3 || cell.animals[i].direction == 5 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
	case 5:
		if (in_bounds(soup, i+1,j-1) && len(soup[i+1][j-1].animals) > 0) {
			var cell = soup[i+1][j-1]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction != 1 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
		if (in_bounds(soup, i,j-1) && len(soup[i][j-1].animals) > 0) {
			var cell = soup[i][j-1]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction == 7 || cell.animals[i].direction == 5 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
		if (in_bounds(soup, i+1,j) && len(soup[i+1][j].animals) > 0) {
			var cell = soup[i+1][j]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction == 5 || cell.animals[i].direction == 3 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
	case 7:
		if (in_bounds(soup, i-1,j-1) && len(soup[i-1][j-1].animals) > 0) {
			var cell = soup[i-1][j-1]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction != 3 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
		if (in_bounds(soup, i,j-1) && len(soup[i][j-1].animals) > 0) {
			var cell = soup[i][j-1]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction == 7 || cell.animals[i].direction == 5 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
		if (in_bounds(soup, i-1,j) && len(soup[i-1][j].animals) > 0) {
			var cell = soup[i-1][j]
			for i := 0; i < len(cell.animals); i++ {
				if cell.animals[i].direction == 1 || cell.animals[i].direction == 7 {
					continue
				}
				if cell.animals[i].phylum == "WATER" {
					water_present = true
				} else if cell.animals[i].phylum == "GRASS" {
					grass_present = true
				} else if cell.animals[i].phylum == "FIRE" {
					fire_present = true
				}
			}
		}
	}
	if (water_present && fire_present && grass_present) {
		return phylum
	} else if (phylum == "WATER" && grass_present) {
		return "GRASS"
	} else if (phylum == "FIRE" && water_present) {
		return "WATER"
	} else if (phylum == "GRASS" && fire_present) {
		return "FIRE"
	}
	return phylum
}


func move(soup [][]Cell, i int, j int, direction int) (int, int, int){
	switch direction {
	case 1:
		if is_wall(soup, i-1, j) && is_wall(soup, i, j+1) {
			if !is_wall(soup, i+1, j-1) {
				return i+1,j-1,5
			}
			return i,j,5
		} else if is_wall(soup, i-1, j) {
			if !is_wall(soup, i+1, j+1) {
				return i+1,j+1,3
			}
			return i,j,5
		} else if is_wall(soup, i, j+1) {
			if !is_wall(soup, i-1, j-1) {
				return i-1,j-1,7
			}
			return i,j,5
		} else {
			return i-1,j+1,1
		}
	case 3:
		if is_wall(soup, i+1, j) && is_wall(soup, i, j+1) {
			if !is_wall(soup, i-1, j-1) {
				return i-1,j-1,7
			}
			return i,j,7
		} else if is_wall(soup, i+1, j) {
			if !is_wall(soup, i-1, j+1) {
				return i-1,j+1,1
			}
			return i,j,7
		} else if is_wall(soup, i, j+1) {
			if !is_wall(soup, i+1, j-1) {
				return i+1,j-1,5
			}
			return i,j,7
		} else {
			return i+1,j+1,3
		}
	case 5:
		if is_wall(soup, i+1, j) && is_wall(soup, i, j-1) {
			if !is_wall(soup, i-1, j+1) {
				return i-1,j+1,1
			}
			return i,j,1
		} else if is_wall(soup, i+1, j) {
			if !is_wall(soup, i-1, j-1) {
				return i-1,j-1,7
			}
			return i,j,1
		} else if is_wall(soup, i, j-1) {
			if !is_wall(soup, i+1, j+1) {
				return i+1,j+1,3
			}
			return i,j,1
		} else {
			return i+1,j-1,5
		}
	case 7:
		if is_wall(soup, i-1, j) && is_wall(soup, i, j-1) {
			if !is_wall(soup, i+1, j+1) {
				return i+1,j+1,3
			}
			return i,j,3
		} else if is_wall(soup, i-1, j) {
			if !is_wall(soup, i+1, j-1) {
				return i+1,j-1,5
			}
			return i,j,3
		} else if is_wall(soup, i, j-1) {
			if !is_wall(soup, i-1, j+1) {
				return i-1,j+1,1
			}
			return i,j,3
		} else {
			return i-1,j-1,7
		}
	}
	return i,j,direction
}

func in_bounds(soup [][]Cell, i int, j int) (bool){
	return i >= 0 && i < len(soup) && j < len(soup[i]) && j >= 0
}

func is_wall(soup [][]Cell, i int, j int) (bool){
	return !in_bounds(soup, i, j) || (len(soup[i][j].animals) > 0 && soup[i][j].animals[0].phylum == "WALL")
}

func contains(animals []Animal, _phylum string) bool {
    for _, a := range animals {
        if a.phylum == _phylum {
            return true
        }
    }
    return false
}

